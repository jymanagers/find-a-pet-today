from datetime import datetime

from app import db

class AdoptPetDetails(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nameOwner = db.Column(db.String(), nullable=False)
    email = db.Column(db.String(), nullable=False)
    namePet = db.Column(db.String(), nullable=False)
    typePet = db.Column(db.String(), nullable=False)
    breed = db.Column(db.String(), nullable=False)
    age = db.Column(db.String(), nullable=False)
    isHdbApproved = db.Column(db.Boolean(), nullable=False)
    isNeutered = db.Column(db.Boolean(), nullable=False)
    isVaccinated = db.Column(db.Boolean(), nullable=False)
    size = db.Column(db.String(), nullable=False)
    colour = db.Column(db.String(), nullable=False)
    sickness = db.Column(db.String(), nullable=False)
    about = db.Column(db.String(), nullable=False)
    isAdopted = db.Column(db.Boolean(), nullable=False, default=False)
    postDate = db.Column(db.DateTime(), nullable=False, default=datetime.utcnow)
    adoptDate = db.Column(db.DateTime(), nullable=True)

    def __init__(self, nameOwner, email, namePet, typePet, breed, age, isHdbApproved, isNeutered, isVaccinated, size, colour, sickness, about, isAdopted, postDate, adoptDate):
        self.nameOwner = nameOwner
        self.email = email
        self.namePet = namePet
        self.breed = breed
        self.age = age
        self.isHdbApproved = isHdbApproved
        self.isNeutered = isNeutered
        self.isVaccinated = isVaccinated
        self.size = size
        self.colour = colour
        self.sickness = sickness
        self.about = about
        self.isAdopted = isAdopted
        self.postDate = postDate
        self.adoptDate = adoptDate

    def __repr__(self):
        return '<id: {}>'.format(self.id)