import os

class Config(object):
    # set environment variable when in
    SECRET_KEY = os.environ.get('SECRET_KEY') or '.?[R36CQ+/jbu=<p#:5M'
    UPLOAD_FOLDER = 'static/data/uploads'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')
    MAX_CONTENT_LENGTH = 15 * 1024 * 1024
