import pickle
import requests
import time

from bs4 import BeautifulSoup as bs4

def cws_max_pages(headers):
    print('====Getting CWS max pages')
    url = 'https://www.catwelfare.org/wcbcategories/adopt/'
    resp = requests.get(url, headers=headers)
    soup = bs4(resp.content, 'html.parser')
    max_pages = soup.find('div', {'class': 'wpc-paginations'}).findAll('a')[-2]
    
    return int(max_pages.text)

def cws_get_links(headers, max_pages):
    print('====Getting all page links')
    links = []
    for page in range(1, max_pages+1):
        url = f'https://www.catwelfare.org/wcbcategories/adopt/?page={str(page)}'
        resp = requests.get(url, headers=headers)
        soup = bs4(resp.content, 'html.parser')
        all_links = soup.findAll('a', {'class': 'wpc-product-link'})
        for link in all_links:
            links.append(link['href'])
    
    return links

def cws_get_details(headers, url):
    print('=====Getting details of cat')
    details = {}
    resp = requests.get(url, headers=headers)
    soup = bs4(resp.content, 'html.parser')
    details['image'] = soup.find('div', {'class': 'product-img-view'}).find('img')['src']
    details['name'] = soup.find('h4').text.strip()
    # get image
    info = soup.find('table', {'id': 'wobs-table'}).findAll('tr')
    age = info[0].td.text.strip()
    details['gender'] = info[2].td.text.strip()
    details['breed'] = info[3].td.text.strip()
    details['url'] = url
    return details

def cws_main(headers):    
    max_pages = cws_max_pages(headers)
    links = cws_get_links(headers, max_pages)
    cws_cats = [cws_get_details(headers, link) for link in links]
    
    return cws_cats

def spca_cats_max_pages(headers):
    url = 'http://www.spca.org.sg/services.asp?cat=2'
    resp = requests.get(url, headers=headers)
    soup = bs4(resp.content, 'html.parser')
    last_page = soup.findAll('a', {'class': 'alink'})[-1]
    if 'last'  in last_page.text.lower():
        last_page = last_page['href']
        max_pages = last_page[last_page.find('Page=')+5]
    
    return int(max_pages)

def spca_cats_get_details(animal, url):
    details = {}
    general_info = animal.findAll('td')
    details['image'] = f"http://www.spca.org.sg/{general_info[0].img['src']}"
    info = general_info[2].findAll('tr')
    details['name'] = info[0].text.strip()
    age = info[4].text.strip()
    age = age[age.find(':')+2:]
    details['age'] = age[:age.find('yr')] + 'y/o'
    gender = info[1].text.strip()
    details['gender'] = gender[gender.find(':')+2:]
    breed = info[2].text.strip()
    details['breed'] = breed[breed.find(':')+2:]
    colour = info[3].text.strip()
    details['colour'] = colour[colour.find(':')+2:]
    details['url'] = url

    return details

def spca_cats_main(headers):
    details = []
    max_pages = spca_cats_max_pages(headers)

    for page in range(1, max_pages+1):
        url = 'http://www.spca.org.sg/services.asp?Page={page}&cat=1&view=&senior=&sColorCode='
        resp = requests.get(url)
        soup = bs4(resp.content, 'html.parser')
        animals = soup.findAll('td', {'class': 'content'})[5].findAll('tr')

        for animal in animals:
            info = spca_cats_get_details(animal, url)
            details.append(info)

    return details

def spca_dogs_max_pages(headers):
    url = 'http://www.spca.org.sg/services.asp?cat=1'
    resp = requests.get(url, headers=headers)
    soup = bs4(resp.content, 'html.parser')
    last_page = soup.findAll('a', {'class': 'alink'})[-1]
    if 'last'  in last_page.text.lower():
        last_page = last_page['href']
        max_pages = last_page[last_page.find('Page=')+5]
    
    return int(max_pages)

def spca_dogs_get_details(animal, url):
    details = {}
    general_info = animal.findAll('td')
    details['image'] = f"http://www.spca.org.sg/{general_info[0].img['src']}"
    info = general_info[2].findAll('tr')
    details['name'] = info[0].text.strip()
    age = info[4].text.strip()
    age = age[age.find(':')+2:]
    details['age'] = age[:age.find('yr')] + 'y/o'
    gender = info[1].text.strip()
    details['gender'] = gender[gender.find(':')+2:]
    breed = info[2].text.strip()
    details['breed'] = breed[breed.find(':')+2:]
    colour = info[3].text.strip()
    details['colour'] = colour[colour.find(':')+2:]
    details['is_hdb'] = info[5].text.strip()
    details['url'] = url

    return details

def spca_dogs_main(headers):
    details = []
    max_pages = spca_cats_max_pages(headers)
    for page in range(1, max_pages+1):
        url = 'http://www.spca.org.sg/services.asp?Page={page}&cat=1&view=&senior=&sColorCode='
        resp = requests.get(url)
        soup = bs4(resp.content, 'html.parser')
        animals = soup.findAll('td', {'class': 'content'})[5].findAll('tr')
        for animal in animals:
            info = spca_dogs_get_details(animal, url)
            details.append(info)

    return details

def lkp_get_details(cat):
    details = {}
    details['image'] = cat.find('div', {'class': 'wpb_single_image wpb_content_element vc_align_center kitten-image'}).img['data-lazy-src']
    details['name'] = cat.find('h2').text.strip()
    info = cat.find('div', {'class': 'vc_cta3-content'}).findAll('span')
    gender = info[0].text.strip()
    details['gender'] = gender[gender.find(':')+2:]
    age = info[1].text.strip()
    details['age'] = age[age.find(':')+2:]
    url = cat.find('div', {'class': 'vc_cta3-actions'}).find('a')
    details['url'] = url['href']
    
    return details

def lkp_main(headers):
    url = 'https://www.lovekuchingproject.org/adopt/'
    resp = requests.get(url, headers=headers)
    soup = bs4(resp.content, 'html.parser')
    
    cats = soup.findAll('div', {'class': 'wpb_column vc_column_container vc_col-sm-6 vc_col-xs-12'})
    details = [lkp_get_details(cat) for cat in cats]
    
    return details        

def main():
    headers = {
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Brave Chrome/79.0.3945.130 Mobile Safari/537.36'
    }

    # pkls = ['cws_cats', 'spca_cats', 'spca_dogs', 'lkp']
    pkls = ['cws_cats']

    print('===Getting CWS===')
    cws_cats = cws_main(headers)
    # print('===Getting SPCA CATS===')
    # spca_cats = spca_cats_main(headers)
    # print('===Getting SPCA DOGS===')
    # spca_dogs = spca_dogs_main(headers)
    # print('===Getting LKP===')
    # lkp_cats = lkp_main(headers)

    print('===SAVING PKLS===')
    with open('static/data/cws_cats.pkl', 'wb') as f:
        pickle.dump(cws_cats, f)

main()