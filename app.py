from config import Config
import logging
import math
import os
import pickle
from werkzeug.utils import secure_filename

from flask import Flask, current_app, flash, redirect, request, render_template, url_for
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

from forms import ContactForm, AdoptUploadForm

from FAPManager import FAPManager

app = Flask(__name__, static_url_path='/static')
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)

from models import *

logger = FAPManager()

@app.route('/')
def home():
    pets = AdoptPetDetails.query.filter_by(isAdopted=False).all()
    for pet in pets:
        folderIsExists = os.path.isdir(f'static/data/uploads/adoptions/approved/{pet.id}')
        if folderIsExists:
            image = os.listdir(f'static/data/uploads/adoptions/approved/{pet.id}')[0]
            filepath = f"data/uploads/adoptions/approved/{pet.id}/{image}"
            pet.imagePet = filepath
        else:
            pet.imagePet = None
    if len(pets) < 6:
        with open('static/data/all_pkls.pkl', 'rb') as f:
            petsPkl = pickle.load(f)
        pets.extend(petsPkl)
    return render_template('pages/home.html', data=pets[:6])

@app.route('/adopt/<page>')
def adopt(page=1):
    pets = AdoptPetDetails.query.filter_by(isAdopted=False).all()
    for pet in pets:
        folderIsExists = os.path.isdir(f'static/data/uploads/adoptions/approved/{pet.id}')
        if folderIsExists:
            image = os.listdir(f'static/data/uploads/adoptions/approved/{pet.id}')[0]
            filepath = f"data/uploads/adoptions/approved/{pet.id}/{image}"
            pet.imagePet = filepath
        else:
            pet.imagePet = None
    with open('static/data/all_pkls.pkl', 'rb') as f:
        petsPkl = pickle.load(f)

    pets.extend(petsPkl)

    items_per_page = 18
    total_pages = math.ceil(len(pets)/18)

    if page == 1:
        start_pos = 0
    elif int(page) > int(total_pages):
        return redirect(url_for('adopt', page=total_pages))
    else:
        start_pos = items_per_page * (int(page) - 1)
    return render_template('pages/adopt.html', data=pets[start_pos: start_pos+items_per_page], current_page=int(page), total_pages=int(total_pages))

@app.route('/adopt/cats/<page>')
def adopt_cats(page=1):
    cats = AdoptPetDetails.query.filter_by(typePet="cat", isAdopted=False).all()
    for cat in cats:
        # images = os.listdir(f'static/data/uploads/adoptions/approved/{cat.id}')
        folderIsExists = os.path.isdir(f'static/data/uploads/adoptions/approved/{cat.id}')
        if folderIsExists:
            image = os.listdir(f'static/data/uploads/adoptions/approved/{cat.id}')[0]
            filepath = f"data/uploads/adoptions/approved/{cat.id}/{image}"
            cat.imagePet = filepath
        else:
            cat.imagePet = None
        
    with open('static/data/all_cats.pkl', 'rb') as f:
        catsPkl = pickle.load(f)
    
    cats.extend(catsPkl)

    items_per_page = 18
    total_pages = math.ceil(len(cats)/18)

    if page == 1:
        start_pos = 0
    elif int(page) > int(total_pages):
        return redirect(url_for('adopt_cats', page=total_pages))
    else:
        start_pos = items_per_page * (int(page) - 1)
    return render_template('pages/adopt.html', data=cats[start_pos: start_pos+items_per_page], current_page=int(page), total_pages=int(total_pages))

@app.route('/adopt/dogs/<page>')
def adopt_dogs(page=1):
    dogs = AdoptPetDetails.query.filter_by(typePet="dog", isAdopted=False).all()
    for dog in dogs:
        folderIsExists = os.path.isdir(f'static/data/uploads/adoptions/approved/{dog.id}')
        if folderIsExists:
            image = os.listdir(f'static/data/uploads/adoptions/approved/{dog.id}')[0]
            filepath = f"data/uploads/adoptions/approved/{dog.id}/{image}"
            dog.imagePet = filepath
        else:
            dog.imagePet = None
    with open('static/data/all_dogs.pkl', 'rb') as f:
        dogsPkl = pickle.load(f)
    
    dogs.extend(dogsPkl)

    items_per_page = 18
    total_pages = math.ceil(len(dogs)/18)

    if page == 1:
        start_pos = 0
    elif int(page) > int(total_pages):
        return redirect(url_for('adopt_dogs', page=total_pages))
    else:
        start_pos = items_per_page * (int(page) - 1)
    return render_template('pages/adopt.html', data=dogs[start_pos: start_pos+items_per_page], current_page=int(page), total_pages=int(total_pages))

@app.route('/adopt/upload', methods=['GET', 'POST'])
def adopt_upload():
    form = AdoptUploadForm()
    if form.validate_on_submit():
        flash('Your adoption listing has been sent! You should see it listed on the website latest in the next working day. A follow up email may be sent to {}.'.format(form.email.data))
        # Handling of images
        # folder_path = os.path.join(os.path.join(app.config["UPLOAD_FOLDER"], "adoption/tmp"), form.email.data)
        folder_path = f"{app.config['UPLOAD_FOLDER']}/adoptions/tmp/{form.namePet.data.replace(' ', '_').lower()}_{form.email.data.lower()}"
        os.makedirs(folder_path)
        images = request.files.getlist(form.imagesPet.name)
        for image in images:
            filepath = os.path.join(folder_path, secure_filename(image.filename))
            image.save(filepath)
        # Sending of message to Telegram
        logger.notify_message(f"New adoption listing.\n<b>Name:</b> {form.nameOwner.data}\n<b>Email address:</b> {form.email.data}\n<b>Pet name:</b> {form.namePet.data}\n<b>Type of Pet: </b> {form.typePet.data}\n<b>Breed:</b> {form.breed.data}\n<b>Age:</b> {form.age.data}\n<b>HDB approved:</b> {form.hdbApproved.data}\n<b>Neutered:</b> {form.neutered.data}\n<b>Vaccinated:</b> {form.vaccinated.data}\n<b>Size:</b> {form.size.data}\n<b>Colour:</b> {form.colour.data}\n<b>Sickness:</b> {form.sickness.data}\n<b>About pet:</b> {form.about.data}")
        return redirect(url_for('home'))
    return render_template('pages/adopt_upload.html', form=form)

@app.route('/adopt/details/<pk>')
def adopt_detail(pk):
    pet = AdoptPetDetails.query.filter_by(id=pk).first()
    if os.path.isdir(f'static/data/uploads/adoptions/approved/{pk}'):
        images = [f"data/uploads/adoptions/approved/{pk}/{image}" for image in os.listdir(f'static/data/uploads/adoptions/approved/{pk}')]
    else:
        images = None
    return render_template('pages/adopt_detail.html', data=pet, images=images)

@app.route('/contact', methods=['GET', 'POST'])
def contact():
    form = ContactForm()
    if form.validate_on_submit():
        flash('Your message has been submitted! A follow up reply may be sent to {} if needed.'.format(form.email.data))
        logger.notify_message("New message by {}\nEmail address: {}\nMessage: {}".format(form.name.data, form.email.data, form.message.data))
        return redirect(url_for('home'))
    print(form.errors)
    return render_template('pages/contact.html', form=form)

@app.route('/about')
def about():
    return render_template('pages/about.html')

@app.errorhandler(404)
def invalid_route(e):
    return render_template('pages/error/error404.html', error="Page Not Found", statuscode="404")

@app.errorhandler(413)
def request_entity_too_large(error):
    return render_template('pages/error/error413.html', error="File Upload Size Too Big, Please Keep It Under 5MB", statuscode="413")

if __name__ == '__main__':
    app.run(debug=True)
