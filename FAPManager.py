import json
import base64

import telegram
from telegram.ext import Updater

class FAPManager(object):
    def __init__(self):
        with open("config.json", "r") as file:
            config = json.load(file)
        api = config["api"].split(config["separator"])
        token = base64.b64decode(bytes(api[0], encoding='utf-8')).decode('utf-8')
        chat_id = base64.b64decode(bytes(api[1], encoding='utf-8')).decode('utf-8')
        updater = Updater(token=token, use_context=True)
        self.bot = updater.dispatcher.bot
        self.chat_id = chat_id

    def notify_message(self, message: str):
        self.bot.send_message(self.chat_id, message, parse_mode=telegram.ParseMode.HTML)

    def upload_image(self, fileDirectory: str):
        self.bot.send_photo(self.chat_id, open(fileDirectory, 'rb'))
