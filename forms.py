import os

from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed, FileRequired
from wtforms import BooleanField, MultipleFileField, PasswordField, SelectField, StringField, SubmitField, TextAreaField
from wtforms.validators import DataRequired, Email, ValidationError

class FileSizeLimitValidator(object):
    def __init__(self, size_limit=5, message=None):
        self.size_limit = size_limit * 1024 * 1024
        if not message:
            message = f'Total file size should be below {size_limit}MB'
        self.message = message
    
    def __call__(self, form, field):
        total_size = 0
        for item in field.raw_data:
            total_size += len(item.read())
            item.seek(0)
        if total_size > self.size_limit:
            print(f"Total size of files: {total_size}, exceeds limit")
            raise ValidationError(self.message)

class ContactForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    email = StringField('Email address', validators=[DataRequired(), Email()])
    message = TextAreaField('Message', validators=[DataRequired()])
    submit = SubmitField('Contact')

class AdoptUploadForm(FlaskForm):
    nameOwner = StringField('Contact name', validators=[DataRequired()])
    email = StringField('Email address', validators=[DataRequired(), Email()])
    namePet = StringField('Pet name', validators=[DataRequired()])
    typePet = SelectField('Type of pet', choices=[("dog", "Dog"), ("cat", "Cat"), ("others", "Others")], validators=[DataRequired()])
    imagesPet = FileField('Images', validators=[
        FileSizeLimitValidator(),
        FileRequired(),
        FileAllowed(['jpg', 'jpeg', 'png'], 'Invalid file type. Must be .jpg or .jpeg or .png'),
    ])
    breed = StringField('Breed', validators=[DataRequired()])
    age = StringField('Age', validators=[DataRequired()])
    hdbApproved = BooleanField('HDB Approved')
    neutered = BooleanField('Neutered')
    vaccinated = BooleanField('Vaccinated')
    size = StringField('Size', validators=[DataRequired()])
    colour = StringField('Colour', validators=[DataRequired()])
    sickness = TextAreaField('Sickness', validators=[DataRequired()])
    about = TextAreaField('About pet', validators=[DataRequired()])
    submit = SubmitField('Send')
